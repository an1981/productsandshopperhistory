﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace WooliesX.Tests.Utils
{
    public static class HttpClientExtensions
    {
        public static Task<T> GetAndDeserialize<T>(this HttpClient client, string requestUri)
        {
            return client.GetFromJsonAsync<T>(requestUri);
        }
        public static async Task<T> GetAndDeserialize<T>(this HttpResponseMessage message)
        {
            message.EnsureSuccessStatusCode();
            return  JsonConvert.DeserializeObject<T>(await message.Content.ReadAsStringAsync());

        }
    }
}
