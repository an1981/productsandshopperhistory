﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using WooliesX;

namespace WooliesX.Tests.Fixtures
{
    public class ApiWebApplicationFactory: WebApplicationFactory<WebAPI.Startup>
    {
        public IConfiguration Configuration { get; private set; }

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureAppConfiguration(config =>
            {
                Configuration = new ConfigurationBuilder()
                  .AddJsonFile("appsettings.local.test.json")
                  .Build();

                config.AddConfiguration(Configuration);
            });

 
        }
    }
}
