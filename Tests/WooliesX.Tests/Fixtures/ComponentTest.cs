﻿
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Xunit;

namespace WooliesX.Tests.Fixtures
{
    public class ComponentTest : IClassFixture<ApiWebApplicationFactory>
    {
        protected readonly ApiWebApplicationFactory _factory;
        protected readonly HttpClient _client;
        
        public ComponentTest(ApiWebApplicationFactory fixture)
        {
             
            _factory = fixture;
            _client = _factory.CreateClient();
        }
    }
}
