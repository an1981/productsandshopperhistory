﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WooliesX.Tests.Fixtures;
using Xunit;
using WooliesX.Tests.Utils;
using WooliesX.Common.Contracts;

namespace WooliesX.Tests.Tests
{
    public class UserControllerTests : ComponentTest
    {
        string userUrl = "/api/user";
        public UserControllerTests(ApiWebApplicationFactory factory) 
            : base(factory)
        {
            
        }
        [Fact]
        public async Task Get_Should_Have_Status_Of_Ok()
        {
            var response = await _client.GetAsync(userUrl);
            response.StatusCode.Should().Be(HttpStatusCode.OK);


        }
        [Fact]
        public async Task Get_Should_Return_User()
        {
            var user = await _client.GetAndDeserialize<User>(userUrl);
            //Assert.IsType<User>(user);
            user.GetType().Should().Be(typeof(User));
            //pick from config file and check
            user.Name.Should().Be("testuser");
            user.Token.Should().Be("b684f5fc-9dbc-4389-9cea-db595c200b47"); 
        }

    }
}
