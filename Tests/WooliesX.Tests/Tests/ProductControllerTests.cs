﻿using FluentAssertions;
using System.Net;
using System.Threading.Tasks;
using WooliesX.Tests.Fixtures;
using WooliesX.Tests.Stubs;
using Xunit;

namespace WooliesX.Tests.Tests
{
    public class ProductControllerTests: ComponentTest
    {
        string productsUrl = "/api/products";
        public ProductControllerTests(ApiWebApplicationFactory factory)
              : base(factory)
        {

        }
        [Fact]
        public async Task Get_All_Products_Should_Return_Not_Found_WhenNoProducts()
        {
            var client = StubHelpers.GetHttpClientWithNoProductsAndShopperHistory(_factory);
            var response = await client.GetAsync(productsUrl);
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }
        [Fact]
        public async Task Get_All_Products_Should_OK_Found_WhenProducts()
        {
            var client = StubHelpers.GetHttpClientProductsAndShopperHistory(_factory);
            var response = await client.GetAsync(productsUrl);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
    }
}
