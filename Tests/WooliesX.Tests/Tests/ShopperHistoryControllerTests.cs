﻿using FluentAssertions;
using System.Net;
using System.Threading.Tasks;
using WooliesX.Tests.Fixtures;
using WooliesX.Tests.Stubs;
using Xunit;

namespace WooliesX.Tests.Tests
{
    public class ShopperHistoryControllerTests : ComponentTest
    {
        string shopperHistoryUrl = "/api/shopperhistory";
        public ShopperHistoryControllerTests(ApiWebApplicationFactory factory)
              : base(factory)
        {

        }
        [Fact]
        public async Task Get_All_ShopperHistory_Should_Return_Not_Found_WhenNoShopperHistory()
        {
            var client = StubHelpers.GetHttpClientWithNoProductsAndShopperHistory(_factory);
            var response = await client.GetAsync(shopperHistoryUrl);
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }
        [Fact]
        public async Task Get_All_ShopperHistory_Should_OK_Found_WhenShopperHistory()
        {
            var client = StubHelpers.GetHttpClientProductsAndShopperHistory(_factory);
            var response = await client.GetAsync(shopperHistoryUrl);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
    }
}
