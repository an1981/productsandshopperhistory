﻿using FluentAssertions;
using System.Net;
using System.Threading.Tasks;
using WooliesX.Tests.Fixtures;
using WooliesX.Tests.Stubs;
using Xunit;
using WooliesX.Tests.Utils;
using System.Collections.Generic;
using WooliesX.APIClient.WooliesXDev.Models;
using System.Linq;

namespace WooliesX.Tests.Tests
{
    public class SortControllerTests : ComponentTest
    {
        string invalidSortUrl = "/api/sort";
        string sortByPriceAsc = "/api/sort?sortOption=Low";
        string sortByPriceDesc = "/api/sort?sortOption=High";
        string sortByNameAsc = "/api/sort?sortOption=Ascending";
        string sortByNameDesc = "/api/sort?sortOption=Descending";
        string sortByRecommendation = "/api/sort?sortOption=Recommended";
        string invalidSortOption = "/api/sort?sortOption=Unknown";
        public SortControllerTests(ApiWebApplicationFactory factory)
              : base(factory)
        {

        }
        [Fact]
        public async Task Sort_All_Should_Return_Bad_Request_WhenNoSortOption()
        {
            var client = StubHelpers.GetHttpClientWithNoProductsAndShopperHistory(_factory);
            var response = await client.GetAsync(invalidSortUrl);
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }
        [Fact]
        public async Task Sort_All_Should_Return_Not_Found_WhenNoProducts()
        {
            var client = StubHelpers.GetHttpClientWithNoProductsAndShopperHistory(_factory);
            var response = await client.GetAsync(sortByPriceAsc);
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }
        [Fact]
        public async Task Sort_All_Should_Return_Not_Found_WhenNoShopperHistory()
        {
            var client = StubHelpers.GetHttpClientWithNoProductsAndShopperHistory(_factory);
            var response = await client.GetAsync(sortByRecommendation);
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task Sort_All_Should_Return_Bad_Request_When_InvalidSortOption()
        {
            var client = StubHelpers.GetHttpClientProductsAndShopperHistory(_factory);
            var response = await client.GetAsync(invalidSortOption);
            //Filters are invoked 
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            
        }

        [Fact]
        public async Task Sort_All_Should_SortOnPriceAsc_WhenProductsFound()
        {
            var client = StubHelpers.GetHttpClientProductsAndShopperHistory(_factory);
            var response = await client.GetAsync(sortByPriceAsc);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var productsResponse = await response.GetAndDeserialize<IReadOnlyCollection<Product>>();
            List<Product> products = new List<Product>(productsResponse);
            products[0].Price.Should().Be(5.1M);
            products[1].Price.Should().Be(10.01M);
            products[2].Price.Should().Be(99.1M);
        }
        [Fact]
        public async Task Sort_All_Should_SortOnPriceDesc_WhenProductsFound()
        {
            var client = StubHelpers.GetHttpClientProductsAndShopperHistory(_factory);
            var response = await client.GetAsync(sortByPriceDesc);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var productsResponse = await response.GetAndDeserialize<IReadOnlyCollection<Product>>();
            List<Product> products = new List<Product>(productsResponse);
            products[0].Price.Should().Be(99.1M);
            products[1].Price.Should().Be(10.01M);
            products[2].Price.Should().Be(5.1M);
     
        }

        [Fact]
        public async Task Sort_All_Should_SortOnNameDesc_WhenProductsFound()
        {
            var client = StubHelpers.GetHttpClientProductsAndShopperHistory(_factory);
            var response = await client.GetAsync(sortByNameDesc);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var productsResponse = await response.GetAndDeserialize<IReadOnlyCollection<Product>>();
            List<Product> products = new List<Product>(productsResponse);
            products[0].Name.Should().Be("Product 3");
            products[1].Name.Should().Be("Product 2");
            products[2].Name.Should().Be("Product 1");
        }
        [Fact]
        public async Task Sort_All_Should_SortOnNameAsc_WhenProductsFound()
        {
            var client = StubHelpers.GetHttpClientProductsAndShopperHistory(_factory);
            var response = await client.GetAsync(sortByNameAsc);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var productsResponse = await response.GetAndDeserialize<IReadOnlyCollection<Product>>();
            List<Product> products = new List<Product>(productsResponse);
            products[0].Name.Should().Be("Product 1");
            products[1].Name.Should().Be("Product 2");
            products[2].Name.Should().Be("Product 3");
        }

        [Fact]
        public async Task Sort_All_Should_SortOnQuantity_WhenProductsFound()
        {
            var client = StubHelpers.GetHttpClientProductsAndShopperHistory(_factory);
            var response = await client.GetAsync(sortByRecommendation);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var shopperHistoryResponse = await response.GetAndDeserialize<IReadOnlyCollection<ShopperHistory>>();
             
            var firstShopperHistory = shopperHistoryResponse.ElementAt(0);
            var products = new List<Product>(firstShopperHistory.Products);
            products[0].Quantity.Should().Be(10);
            products[1].Quantity.Should().Be(20);
            products[2].Quantity.Should().Be(30);
        }
    }
     
}
