﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using WooliesX.APIClient.WooliesXDev.Models;
using WooliesX.APIClient.WooliesXDev.ServiceAcessor;

namespace WooliesX.Tests.Stubs
{
    public class ValidProductsAndShopperHistory : IProductAndShopper
    {
        public async Task<IReadOnlyCollection<Product>> GetAllProducts(Guid token)
        {
            List<Product> lstProducts = GetProductsList();
            ReadOnlyCollection<Product> products = new ReadOnlyCollection<Product>(lstProducts);
            return await Task.FromResult(products);
        }

        public async Task<IReadOnlyCollection<ShopperHistory>> GetAllShopperHistory(Guid token)
        {
            List<ShopperHistory> lstShopperHistory = new List<ShopperHistory>();
            lstShopperHistory.Add(new ShopperHistory()
            {
                CustomerId = 1,
                Products = GetProductsList()
            });
            lstShopperHistory.Add(new ShopperHistory()
            {
                CustomerId = 2,
                Products = GetProductsList()
            });
            ReadOnlyCollection<ShopperHistory> shopperHistory = new ReadOnlyCollection<ShopperHistory>(lstShopperHistory);
            return await Task.FromResult(shopperHistory);
        }
        private static List<Product> GetProductsList()
        {
            List<Product> lstProducts = new List<Product>();
            lstProducts.Add(new Product() { Name = "Product 2", Price = 10.01M, Quantity = 10 });
            lstProducts.Add(new Product() { Name = "Product 1", Price = 99.1M, Quantity = 20 });
            lstProducts.Add(new Product() { Name = "Product 3", Price = 5.1M, Quantity = 30 });
            return lstProducts;
        }
    }
}
