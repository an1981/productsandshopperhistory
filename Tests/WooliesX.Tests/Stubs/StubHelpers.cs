﻿using System;
 
using System.Net.Http;
using Microsoft.Extensions.DependencyInjection;
using WooliesX.APIClient.WooliesXDev.ServiceAcessor;
using WooliesX.BusinessLogic.Services;
using WooliesX.Tests.Fixtures;

namespace WooliesX.Tests.Stubs
{
    public static class StubHelpers
    {
        public static HttpClient GetHttpClientWithNoProductsAndShopperHistory(ApiWebApplicationFactory factory)
        {
            var client = factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddTransient<IProductsAndShopperService, NoProductsAndShopperHistoryReturnStub>();
                });
            })
           .CreateClient();
            return client;
        }
        public static HttpClient GetHttpClientProductsAndShopperHistory(ApiWebApplicationFactory factory)
        {
            var client = factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddTransient<IProductAndShopper, ValidProductsAndShopperHistory>();
                });
            })
           .CreateClient();
            return client;
        }
    }
}
