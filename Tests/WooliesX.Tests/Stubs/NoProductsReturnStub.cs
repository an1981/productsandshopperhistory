﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using WooliesX.BusinessLogic;
using WooliesX.BusinessLogic.Services;
using WooliesX.Common.Contracts;

namespace WooliesX.Tests.Stubs
{
    public class NoProductsAndShopperHistoryReturnStub : IProductsAndShopperService
    {
        public async Task<IReadOnlyCollection<Product>> GetAllProducts(Guid token)
        {
            List<Product> lstSource = new List<Product>();
            ReadOnlyCollection<Product> products = new ReadOnlyCollection<Product>(lstSource);
            return await Task.FromResult( products);
        }

        public async Task<IReadOnlyCollection<ShopperHistory>> GetAllShopperHistory(Guid token)
        {
            List<ShopperHistory> lstSource = new List<ShopperHistory>();
            ReadOnlyCollection<ShopperHistory> shopper = new ReadOnlyCollection<ShopperHistory>(lstSource);
            return await Task.FromResult(shopper);
        }

        public Task<IReadOnlyCollection<Product>> SortAllProducts(Guid token, SortOption sortOption)
        {
            return GetAllProducts(token);
        }

        public Task<IReadOnlyCollection<ShopperHistory>> SortAllShopperHistory(Guid token, SortOption sortOption)
        {
            return GetAllShopperHistory(token);
        }
    }
}
