﻿using System;

namespace WooliesX.Common
{
    public static class ErrorCode
    {
        public static readonly string Invalid_Sort_Option = "INVALID_SORT_OPTION";
        public static readonly string Unknown_API_Client_Issue = "UNKNOWN_API_CLIENT_ISSUE";
    }
}
