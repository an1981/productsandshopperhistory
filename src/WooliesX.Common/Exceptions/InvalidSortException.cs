﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WooliesX.Common.Exceptions
{
    public class InvalidSortException : WooliesXException
    {
        public InvalidSortException(string message) :
            base(message)
        {
            Code = ErrorCode.Invalid_Sort_Option;
        }
    }
}
