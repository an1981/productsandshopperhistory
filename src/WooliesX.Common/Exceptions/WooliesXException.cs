﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WooliesX.Common.Exceptions
{
    public class WooliesXException : Exception
    {
        public WooliesXException(string message)
            : base(message)
        {

        }
        public string Code { get; protected set; }
    }
}
