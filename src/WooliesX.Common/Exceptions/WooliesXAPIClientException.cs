﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WooliesX.Common.Exceptions
{
    public class WooliesXAPIClientException: WooliesXException
    {
		public WooliesXAPIClientException(string requestUri, int statusCode, string method, string message) 
			: base(message +$" for the URI: {requestUri} with Status Code {statusCode} HttpMethod {method}" )
		{
			Code = ErrorCode.Unknown_API_Client_Issue;
		}
	}
}
