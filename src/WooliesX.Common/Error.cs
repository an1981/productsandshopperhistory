﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WooliesX.Common
{
    public class Error
    {
        public string Code { get; set; }
        public string Message { get; set; }
    }
}
