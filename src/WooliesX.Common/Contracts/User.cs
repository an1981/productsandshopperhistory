﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WooliesX.Common.Contracts
{
    public class User
    {
        public string Name { get; set; }
        public string Token { get; set; }

    }
}
