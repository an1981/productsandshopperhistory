﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Payload;

namespace WooliesX.WebAPI
{
    
    public static class ServiceCollectionExtensions
    {
        public static void AddMappingProfiles(this IServiceCollection services)
        {
            services.AddSingleton(provider => new MapperConfiguration(cfg =>
            {

                cfg.AddProfile<BusinessPayloadMappingProfile>();

            }).CreateMapper());
        }
    }
}
