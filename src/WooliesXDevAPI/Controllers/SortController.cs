﻿ 
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WooliesX.BusinessLogic;
using WooliesX.BusinessLogic.Services;
using WooliesX.BusinessLogic.Stratergy;

namespace WooliesX.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SortController : ControllerBase
    {
        private readonly IProductsAndShopperService _productsAndShopper;
        private readonly ILogger<SortController> _logger;
        private readonly IConfiguration _configuration;
        private readonly IProductsSortStratergy _productsSortStratergy;
        public SortController(ILogger<SortController> logger,
            IProductsAndShopperService productsAndShopper,
            IConfiguration configuration,
            IProductsSortStratergy productsSortStratergy)
        {
            _productsAndShopper = productsAndShopper;
            _logger = logger;
            _configuration = configuration;
            _productsSortStratergy = productsSortStratergy;
        }

        [HttpGet]
        public async Task<IActionResult> SortProducts([FromQuery, Required] SortOption sortOption)
        {
            
            

            _logger.LogInformation($"Sort products called with sort option {sortOption.ToString()}");
            if (sortOption == SortOption.Recommended)
            {
                _logger.LogInformation($"calling the shopperHistory query");
                var shopperHistory = await _productsAndShopper.SortAllShopperHistory(Guid.Parse(_configuration["Token"]), sortOption);
                if (shopperHistory.Count > 0)
                {
                    _logger.LogInformation($"returns ok with sorted shopper history data");
                    return Ok(shopperHistory);
                }
                _logger.LogInformation($"returns notfound with no sorted shopper history data");

                return NotFound();
            }
            else
            {
                _logger.LogInformation($"calling the sortallproducts query");
                 var products = await _productsSortStratergy.SortProducts(Guid.Parse(_configuration["Token"]), sortOption);
                if (products.Count > 0)
                {
                    _logger.LogInformation($"returns ok with sorted products data");

                    return Ok(products);

                }
                _logger.LogInformation($"returns not found with no sorted shopper history data");

                return NotFound();
            }
        }
    }
}
     
