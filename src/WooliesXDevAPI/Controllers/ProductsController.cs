﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooliesX.APIClient.WooliesXDev.ServiceAcessor;
using WooliesX.BusinessLogic.Services;

namespace WooliesX.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductsAndShopperService _productAndShopperService;
        private readonly IConfiguration _configuration;
        public ProductsController(IProductsAndShopperService productsAndShopperService,
            IConfiguration configuration
            )
        {
            _productAndShopperService = productsAndShopperService;
            _configuration = configuration;
        }
        /// <summary>
        /// Gets all the products
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {   
            var data = await _productAndShopperService.GetAllProducts(Guid.Parse(_configuration["Token"]));
            if (data.Count > 0)
            {
                return Ok(data);
            }
            return NotFound();
        }
    }
}
