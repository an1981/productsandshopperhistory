﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Services;

namespace WooliesX.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShopperHistoryController : ControllerBase
    {
        private readonly IProductsAndShopperService _productAndShopperService;
        private readonly IConfiguration _configuration;
        public ShopperHistoryController(IProductsAndShopperService productsAndShopperService,
            IConfiguration configuration
            )
        {
            _productAndShopperService = productsAndShopperService;
            _configuration = configuration;
        }
        /// <summary>
        /// Gets all the Shopper history
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var data = await _productAndShopperService.GetAllShopperHistory(Guid.Parse(_configuration["Token"]));
            if (data.Count > 0)
            {
                return Ok(data);
            }
            return NotFound();
        }
    }
}
