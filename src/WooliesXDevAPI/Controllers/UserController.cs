﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Services;

namespace WooliesX.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ILogger<UserController> _logger;
        public UserController(IUserService userService, ILogger<UserController> logger)
        {
            _userService = userService;
            _logger = logger;

        }
        [HttpGet]
        public IActionResult GetUser()
        {
            // Not part of CQRS since this not part of the original microservice i feel. 
            _logger.LogInformation("calling the get user");
            var tokenUser = _userService.GetUser();
            return Ok(tokenUser);
        }
    }
}
