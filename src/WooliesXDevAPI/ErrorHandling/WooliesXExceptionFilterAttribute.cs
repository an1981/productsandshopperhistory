﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooliesX.Common;
using WooliesX.Common.Exceptions;

namespace WooliesX.WebAPI.ErrorHandling
{
    public class WooliesXExceptionFilterAttribute : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            var error = context.Exception as WooliesXException;
            if (error != null)
            {

                var errorOut = new Error()
                {
                    Code = error.Code,
                    Message = error.Message
                };


                context.Result = new BadRequestObjectResult(errorOut);
            }
        }
    }
}
