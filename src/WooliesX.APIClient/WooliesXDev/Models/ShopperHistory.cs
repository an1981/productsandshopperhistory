﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WooliesX.APIClient.WooliesXDev.Models
{
    public class ShopperHistory
    {
        public int CustomerId { get; set; }
        public IReadOnlyCollection<Product> Products { get; set; }
    }
}
