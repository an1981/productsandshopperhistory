﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WooliesX.APIClient.WooliesXDev.Models;

namespace WooliesX.APIClient.WooliesXDev.ServiceAcessor
{
    public interface IProductAndShopper
    {
        Task<IReadOnlyCollection<Product>> GetAllProducts(Guid token);
        Task<IReadOnlyCollection<ShopperHistory>> GetAllShopperHistory(Guid token);

    }
}
