﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using WooliesX.APIClient.WooliesXDev.Models;
 

using WooliesX.APIClient.WooliesXDev.HttpClients;
using Microsoft.Extensions.Options;
using WooliesX.APIClient.WooliesXDev.ServiceAcessor;

namespace WooliesX.APIClient.WooliesXDev.ServiceAcessor
{
    public class ProductAndShopperAPIClient :  IProductAndShopper
    {
        private readonly IWooliesXApiClient _apiClient;
        private readonly ILogger<ProductAndShopperAPIClient> _logger;
        private readonly IOptionsMonitor<ProductAndShopperOptions> _options;
        private HttpResponseMessage Response { get; set; }
        public ProductAndShopperAPIClient(IWooliesXApiClient apiClient, 
            ILogger<ProductAndShopperAPIClient> logger,
            IOptionsMonitor<ProductAndShopperOptions> options)
        
        {
            _apiClient = apiClient ;
            _logger = logger;
            _options = options;
        }

        public async Task<IReadOnlyCollection<Product>> GetAllProducts(Guid token)
        {
            if(token == Guid.Empty)
            {
                throw new ArgumentException($"token cannot be null or empty {token}");
            }
            return await _apiClient.GetAsyncList<Product>(string.Format( _options.CurrentValue.Products, token));
             
        }
        
        public async Task<IReadOnlyCollection<ShopperHistory>> GetAllShopperHistory(Guid token)
        {
            if (token == Guid.Empty)
            {
                throw new ArgumentException($"token cannot be null or empty {token}");
            }
            return await _apiClient.GetAsyncList<ShopperHistory>(string.Format( _options.CurrentValue.ShopperHistory,token));
        }
    }
}
