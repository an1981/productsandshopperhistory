﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WooliesX.APIClient.WooliesXDev.ServiceAcessor
{
    public class ProductAndShopperOptions
    {
        public string Products { get; set; }
        public string ShopperHistory { get; set; }
    }
}
