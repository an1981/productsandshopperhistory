﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WooliesX.APIClient.WooliesXDev.HttpClients
{
    public interface IWooliesXApiClient
    {
        Task<IReadOnlyCollection<T>> GetAsyncList<T>(string requestUrl) where T  : new();
    }
}
