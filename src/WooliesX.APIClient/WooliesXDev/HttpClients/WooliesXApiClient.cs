﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WooliesX.Common.Exceptions;
using System.Text.Json;
namespace WooliesX.APIClient.WooliesXDev.HttpClients
{
    public class WooliesXApiClient : IWooliesXApiClient
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger<WooliesXApiClient> _logger;
        private HttpResponseMessage _response { get; set; }
        public WooliesXApiClient(HttpClient httpClient, ILogger<WooliesXApiClient> logger)
        {
            //configured by polly for retry and transient error handler
            _httpClient = httpClient;
            _logger = logger;
        }
        public async Task<IReadOnlyCollection<T>> GetAsyncList<T>(string requestUrl) where T : new()
        {
            
            _response = await _httpClient.GetAsync(requestUrl);
            return await ReadResponse<T>();
        }
        private async Task<IReadOnlyCollection<T>> ReadResponse<T>() where T : new()
        {
            if (_response.IsSuccessStatusCode)
            {
                _logger.LogInformation("Got the success code ");
                
                var result = await JsonSerializer.DeserializeAsync<IReadOnlyCollection<T>>
                    (await _response.Content.ReadAsStreamAsync(),
                    //Setting to camelcaseing since the api returns. 
                    new JsonSerializerOptions() { 
                         PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                    });
                return result;
            }
            else
            {
                var errorMessage = $"Could not get successful response from URI:{_response.RequestMessage.RequestUri}, Method:{_response.RequestMessage.Method}, Status:{_response.StatusCode.ToString()}.";
                _logger.LogError(errorMessage);
                throw new WooliesXAPIClientException(_response.RequestMessage.RequestUri.ToString(), (int)_response.StatusCode, _response.RequestMessage.Method.ToString(), "Failed to get the data");

            }
        }
    }
}
