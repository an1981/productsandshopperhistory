﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using WooliesX.APIClient.WooliesXDev;
using System.Net.Http;
using Polly.Extensions.Http;
using Polly;
using WooliesX.APIClient.WooliesXDev.HttpClients;
using WooliesX.APIClient.WooliesXDev.ServiceAcessor;

namespace WooliesX.APIClient
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApiClient(this IServiceCollection services, IConfiguration configuration )
        {
            var retryPolicy = HttpPolicyExtensions
                   .HandleTransientHttpError() 
                   .WaitAndRetryAsync(Convert.ToInt32(configuration["RetryCount"]),
                   retryAttempt => TimeSpan.FromSeconds(retryAttempt));
            services.AddHttpClient<IWooliesXApiClient, WooliesXApiClient>(o =>
                          o.BaseAddress = new Uri(configuration["BaseUrl"]))
                           .AddPolicyHandler(retryPolicy);
            services.AddScoped<IProductAndShopper, ProductAndShopperAPIClient>();
            services.Configure<ProductAndShopperOptions>(options => configuration.GetSection("WooliesXResourceUrl").Bind(options));
            return services;
        }
    }
}
