﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace WooliesX.BusinessLogic
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum SortOption
    {
        High,
        Low,
        Ascending,
        Descending,
        Recommended,
        Unknown
    }
}
