﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using WooliesX.APIClient.WooliesXDev.Models;

namespace WooliesX.BusinessLogic.Payload
{
    public class BusinessPayloadMappingProfile:Profile
    {
        public BusinessPayloadMappingProfile()
        {
            CreateMap<Product, Common.Contracts.Product>().ReverseMap();
            CreateMap<ShopperHistory, Common.Contracts.ShopperHistory>().ReverseMap();
        }
    }
}
