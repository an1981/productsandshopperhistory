﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Services;
using WooliesX.Common.Contracts;

namespace WooliesX.BusinessLogic.Stratergy
{
    public class SortByName : ISortProducts
    {
        public SortOption sortOption => SortOption.Ascending; private readonly IProductsAndShopperService _productAndShopper;
        public SortByName(IProductsAndShopperService productAndShopper)
        {
            _productAndShopper = productAndShopper;
        }
        public async Task<IEnumerable<Product>> SortProducts(Guid token)
        {
            var products = await _productAndShopper.GetAllProducts(token);
            return products.SortByNameAsc();
        }
    }
}
