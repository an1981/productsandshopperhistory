﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WooliesX.Common.Contracts;

namespace WooliesX.BusinessLogic.Stratergy
{
    public class ProductsSortStratgergy : IProductsSortStratergy
    {
        private readonly IEnumerable<ISortProducts> _operators;
        public ProductsSortStratgergy(IEnumerable<ISortProducts> operators)
        {
            _operators = operators;
        }
        public async Task<IReadOnlyCollection<Product>> SortProducts(Guid token,SortOption option)
        {
          var product =  await _operators.FirstOrDefault(s => s.sortOption == option).SortProducts(token);
          // Just applying this shortcut in the interest of time 
            var readOnlyColl = new List<Product>(product).AsReadOnly();
          return readOnlyColl;
        }
    }
    public interface IProductsSortStratergy
    {
         Task<IReadOnlyCollection<Product>> SortProducts(Guid token,SortOption option);
    }
}
