﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WooliesX.APIClient.WooliesXDev.ServiceAcessor;
using WooliesX.Common.Contracts;
using WooliesX.BusinessLogic;
using WooliesX.BusinessLogic.Services;

namespace WooliesX.BusinessLogic.Stratergy
{
    public class SortByPriceAsc : ISortProducts
    {
        private readonly IProductsAndShopperService _productAndShopper;
        public SortByPriceAsc(IProductsAndShopperService productAndShopper)
        {
            _productAndShopper = productAndShopper;
        }
        public SortOption sortOption => SortOption.High;

        public async Task<IEnumerable<Product>> SortProducts(Guid token)
        {
            var products = await _productAndShopper.GetAllProducts(token);
            return products.SortByPriceAsc();
            
        }
    }
}
