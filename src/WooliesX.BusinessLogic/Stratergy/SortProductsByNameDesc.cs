﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WooliesX.BusinessLogic.Services;
using WooliesX.Common.Contracts;

namespace WooliesX.BusinessLogic.Stratergy
{
   
    public class SortByNameDesc : ISortProducts
    {
        public SortOption sortOption => SortOption.Descending;

        private readonly IProductsAndShopperService _productAndShopper;
        public SortByNameDesc(IProductsAndShopperService productAndShopper)
        {
            _productAndShopper = productAndShopper;
        }
        public async Task<IEnumerable<Product>> SortProducts(Guid token)
        {
            var products = await _productAndShopper.GetAllProducts(token);
            return products.SortByNameDesc();
        }
    }
}
