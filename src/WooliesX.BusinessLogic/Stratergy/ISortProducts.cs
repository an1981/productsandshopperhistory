﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WooliesX.Common.Contracts;

namespace WooliesX.BusinessLogic.Stratergy
{
    public interface ISortProducts
    {
        SortOption sortOption { get; }
         Task<IEnumerable<Product>> SortProducts(Guid token);
    }
  
}
