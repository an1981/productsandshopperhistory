﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WooliesX.APIClient.WooliesXDev.ServiceAcessor;
using WooliesX.Common.Contracts;
using WooliesX.Common.Exceptions;

namespace WooliesX.BusinessLogic.Services
{
    public class ProductsAndShopperService : IProductsAndShopperService
    {
        private readonly IProductAndShopper _apiClient;
        private readonly ILogger<ProductsAndShopperService> _logger;
        private IMapper _mapper;
        public ProductsAndShopperService(IProductAndShopper apiClient,
            ILogger<ProductsAndShopperService> logger,
            IMapper mapper)
        {
            _logger = logger;
            _apiClient = apiClient;
            _mapper = mapper;
        }

        public async Task<IReadOnlyCollection<Product>> GetAllProducts(Guid token)
        {
            RaiseTokenNullExceptionIfEmpty(token);

            var products = await _apiClient.GetAllProducts(token);
            return _mapper.Map<IReadOnlyCollection<Product>>(products);
        }

        public async Task<IReadOnlyCollection<ShopperHistory>> GetAllShopperHistory(Guid token)
        {
            RaiseTokenNullExceptionIfEmpty(token);
            var shopperHistory = await _apiClient.GetAllShopperHistory(token);
            return _mapper.Map<IReadOnlyCollection<ShopperHistory>>(shopperHistory);

        }

        public async Task<IReadOnlyCollection<Product>> SortAllProducts(Guid token, SortOption sortOption)
        {
            RaiseTokenNullExceptionIfEmpty(token);

            _logger.LogInformation($"In the Sort All Products Handler with Sort Option {sortOption.ToString()}");
            if ((sortOption == SortOption.Low) || (sortOption == SortOption.High) || (sortOption == SortOption.Ascending) || (sortOption == SortOption.Descending))
            {
                _logger.LogInformation($"calling the products resource for all the products");
                var products = await GetAllProducts(token);
                _logger.LogInformation($"After the products returns the response");
                IReadOnlyCollection<Product> sortedProducts = null;
                if (products?.Count>0)
                {
                    _logger.LogInformation($"Products has valid entries");
                    if ((sortOption == SortOption.Low) || (sortOption == SortOption.High))
                    {
                        _logger.LogInformation($"Sorting products on price now");
                        sortedProducts = products.SortByPrice(sortOption);
                    }
                    if ((sortOption == SortOption.Ascending) || (sortOption == SortOption.Descending))
                    {
                        _logger.LogInformation($"Sorting products on Name now");
                        sortedProducts = products.SortByName(sortOption);
                    }
                    products = sortedProducts;
                }

                return products;
            }
            else
            {
                _logger.LogError($"Sort Option {sortOption} is not valid");
                throw new InvalidSortException($"Sort Option {sortOption} is not valid");
            }
        }

        public async Task<IReadOnlyCollection<ShopperHistory>> SortAllShopperHistory(Guid token, SortOption sortOption)
        {
            RaiseTokenNullExceptionIfEmpty(token);
            _logger.LogInformation($"In the Sort All Shopping History Handler with Sort Option {sortOption.ToString()}");
            if (sortOption == SortOption.Recommended)
            {
                _logger.LogInformation("calling the Shopper history resource");
                // calling the shopperHistory resource . Here we call the appropriate query handler and then do the sorting
                
                var shoppingHistoryResponse = await GetAllShopperHistory(token);
                _logger.LogInformation("after the response from the Shopper history resource");
                if (shoppingHistoryResponse?.Count > 0)
                {
                    _logger.LogInformation("response has valid shopping history and sorting the products based on names");
                    //Applying the sorting for product based on the quantity.
                    foreach (var shoppingHistory in shoppingHistoryResponse)
                    {
                        shoppingHistory.Products = shoppingHistory.Products.SortByQuantityAsc();
                    }
                }
                return shoppingHistoryResponse;
            }
            else
            {
                _logger.LogError($"Sort Option {sortOption} is not valid");
                throw new InvalidSortException($"Sort Option {sortOption} is not valid");
            }
        }
        private void RaiseTokenNullExceptionIfEmpty(Guid token)
        {
            if (token == Guid.Empty)
            {
                _logger.LogError($"token is null or empty {token}");

                throw new ArgumentException($"token cannot be null or empty {token}");
            }
        }
    }
}
