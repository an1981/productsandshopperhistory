﻿using System;
using System.Collections.Generic;
using System.Text;
using WooliesX.Common.Contracts;

namespace WooliesX.BusinessLogic.Services
{
    public interface IUserService
    {
        User GetUser();
    }
}
