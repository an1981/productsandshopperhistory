﻿
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using WooliesX.Common.Contracts;

namespace WooliesX.BusinessLogic.Services
{
    public class UserService : IUserService
    {
        private readonly IConfiguration _configuration;
        public UserService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public User GetUser()
        {
            User tokenUser = new User()
            {
                Name = _configuration["Name"],
                Token = _configuration["Token"]
            };
            return tokenUser;
        }
    }
}
