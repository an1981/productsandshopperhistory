﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WooliesX.Common.Contracts;

namespace WooliesX.BusinessLogic.Services
{
    public interface IProductsAndShopperService
    {
        Task<IReadOnlyCollection<ShopperHistory>> SortAllShopperHistory(Guid token, SortOption sortOption);
        Task<IReadOnlyCollection<Product>> SortAllProducts(Guid token,SortOption sortOption);
        Task<IReadOnlyCollection<Product>> GetAllProducts(Guid token);
         Task<IReadOnlyCollection<ShopperHistory>> GetAllShopperHistory(Guid token);

    }
}
