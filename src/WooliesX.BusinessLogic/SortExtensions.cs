﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WooliesX.Common.Contracts;

namespace WooliesX.BusinessLogic
{
    public static class SortExtensions
    {
        public static IReadOnlyCollection<Product> SortByPrice(this IReadOnlyCollection<Product> products, SortOption sortOption)
        {
            if (sortOption == SortOption.Low)
            {
                return products.SortByPriceAsc();
            }
            else if (sortOption == SortOption.High)
            {
                return products.SortByPriceDesc();
            }
            return products;
        }
        public static IReadOnlyCollection<Product> SortByPriceAsc(this IReadOnlyCollection<Product> products)
        {
            return products.OrderBy(s => s.Price).ToList().AsReadOnly();

        }
        public static IReadOnlyCollection<Product> SortByPriceDesc(this IReadOnlyCollection<Product> products)
        {
            return products.OrderByDescending(s => s.Price).ToList().AsReadOnly();

        }
        public static IReadOnlyCollection<Product> SortByQuantityAsc(this IReadOnlyCollection<Product> products)
        {
            return products.OrderBy(s => s.Quantity).ToList();

        }
        public static IReadOnlyCollection<Product> SortByQuantityDesc(this IReadOnlyCollection<Product> products)
        {
            return products.OrderByDescending(s => s.Quantity).ToList();

        }
        public static IReadOnlyCollection<Product> SortByName(this IReadOnlyCollection<Product> products, SortOption sortOption)
        {
            if (sortOption == SortOption.Ascending)
            {
                return products.SortByNameAsc();
            }
            else if (sortOption == SortOption.Descending)
            {
                return products.SortByNameDesc();
            }
            return products;
        }
        public static IReadOnlyCollection<Product> SortByNameAsc(this IReadOnlyCollection<Product> products)
        {
            return products.OrderBy(s => s.Name).ToList();

        }
        public static IReadOnlyCollection<Product> SortByNameDesc(this IReadOnlyCollection<Product> products)
        {
            return products.OrderByDescending(s => s.Name).ToList();

        }
    }
}
