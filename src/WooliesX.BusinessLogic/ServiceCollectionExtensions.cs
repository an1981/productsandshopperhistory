﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using WooliesX.BusinessLogic.Services;
using WooliesX.BusinessLogic.Stratergy;

namespace WooliesX.BusinessLogic
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddBusinessLogicModule(this IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }
            services.AddScoped<IProductsAndShopperService, ProductsAndShopperService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IProductsSortStratergy, ProductsSortStratgergy>();
            services.AddScoped<ISortProducts, SortByName>();
            services.AddScoped<ISortProducts, SortByNameDesc>();
            
            return services;
        }
    }
}
