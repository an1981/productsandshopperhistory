# README #

This project is an aim to deliver the Woolworths Developer Exercise Api V1 hosted on http://dev-wooliesx-recruitment.azurewebsites.net/swagger/#/

The project is hosted on azure and the swagger page is 
https://wooliesx-anup.azurewebsites.net/swagger/index.html
* Quick summary
### What works from the assignment ########
* Excercise 1 and Excercise 2 ( except the sortOption = Recommended)
I have got the unit tests written as part of excercise1 and 2 and they cover the scenarios
Excercise 1 - It returns the user data as in the swagger
Excercise 2 - The SortOption = high,low,ascending,descending works as per the requirement

### What does not work from the assignment ########
Excercise 2 - SortOption = Recommended 
I am not clear what "needs to return based on popularity" means. 

I am calling the shopperhistory api which returns an array of customers and product array.

I have tried all the combinations of sorting for the products
the products based on name, price , quantity both ascending and descending. This does not seem to work.

Excercise 3 - I have a bunch of questions. IF I can get it clarified , would want to attempt it 
1) How do I get the source data if I am not supposed to use the trolley calculator api ? 
2) WHen I try with the payload as per swagger , it works 200 is the result. 
If I key in a payload like this {
  "products": [
    {
      "name": "Test Product A",
      "price": 0
    }
  ],
  "specials": [
    {
      "quantities": [
        {
          "name": "string",
          "quantity": 0
        }
      ],
      "total": 0
    }
  ],
  "quantities": [
    {
      "name": "string",
      "quantity": 0
    }
  ]
} 
the API throws up 500 error. 
I just need a bit of clarity here on what needs to be done here.

* Summary of set up
  -----------------
  To run the project locally set the BaseUrl,Name and Token in the environment variable
  BaseUrl - http://dev-wooliesx-recruitment.azurewebsites.net/api/resource/
  Name - Yourname
  Token - Yourtoken 
  
* ErrorHandling
  ----------------
  For now , I have got the webapi filters which should prevent any 500 errors and translate 
 the errors into more meaningful ones. 


* Logging 
 --------------
 All the logs are just part of the App Service Logs .
  I have not configured the application insights for now due to costing.

* External Dependency of API 
 --------------------------
 The wooliesX dev api being an external entity for the api could be unreachable due to network issues
 There is a retry mechanism using polly which is configured in startup.  

* Configuration
 -----------------
 None. Just use the VS 2019 to run the project

* Dependencies
  ------------ 
  All the dependecies are part of nuget and should be downloaded as part of standard build process 

* Database configuration
 ----------------------
 Not required since all thje data are from the wooliesX APIs.

* How to run tests
 ----------------
 Tests are using the xunit framework along with FluentValidation. 
 The tests are testing the entire component ( from the controller-> business layer and back )
 and this feels more valuable than the traditonal unit testing. 
 Use the Test-> TestExplorer menu from VS to run it or use the dotnet test command

* Deployment instructions
 -----------------------
 The latest is deployed on azure https://wooliesx-anup.azurewebsites.net/swagger/index.html

 To validate the api from http://dev-wooliesx-recruitment.azurewebsites.net/swagger/#/
 use the payload 
 {
 "token": "845a10e8-9b80-410c-a7b4-5610d676e19e",
 "url": "https://wooliesx-anup.azurewebsites.net/api/"
}